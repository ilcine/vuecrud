# Packet Name VueCrud 

[![Latest Version on Packagist](https://ilcine.gitlab.io/img/packagist.svg)](https://packagist.org/packages/emr/vuecrud)
[![License](https://ilcine.gitlab.io/img/mit.svg)](LICENSE.md)

Includes laravel, vuejs, and npm with CRUD (Create, Read, Update, Delete)

- [Laravel Environmet Set](#Packet-name-vuecrud)
    - [Installation](#installation)
    - [Usage](#usage)



_Author:Emrullah İLÇİN_

## Installation

1. Create laravel project // use a project name; ex: proje1 or any

```
composer create-project laravel/laravel proje1
cd proje1
```

2. Install vuecrud

`composer require emr/vuecrud`

3. `edit .env`  // add your user and password into mysql

```
DB_DATABASE=test
DB_USERNAME=test
DB_PASSWORD=test
```

4. Sql table create

`php artisan migrate`

4.1.  if you want  see DB table with "php artisan"

>  `php artisan tinker` # test; See "null" message; because the table is empty; it is OK;

```
>>>Emr\VueCrud\VueCrud::all();
>>>q
```

5. Make publish

created "public/vuecrud/js/app.js" and "app.css"

`php artisan vendor:publish --provider="Emr\VueCrud\VueCrudServiceProvider" --tag=public --force`

> js compiled with "npm run dev"

## Usage

Web server start

ex: `php artisan serve --host=94.177.187.77 --port=8000`

Browser test

ex: `http://94.177.187.77:8000/vuecrud/1`


# Development installation

_Emrullah İLÇİN_

## 1. Create a laravel project.

`composer create-project --prefer-dist laravel/laravel proje1`

`cd $HOME/proje1`

## 2. Create db

add your user and password into mysql and create datbase;

`mysql -uroot -p`  # login prompt `mysql>` or `MariaDB>`

```
create database proje1;
grant all on proje1.* to emr@localhost identified  by "123456";
flush privileges;
exit
```

## 3. edit `.env` 

```
DB_DATABASE=proje1
DB_USERNAME=emr
DB_PASSWORD=123456
```

## 4. create auth 

`php artisan make:auth`


## 5. Install "emr/vuecrud" from packagist

```
composer require emr/vuecrud 
mkdir packages
cp -rf vendor/emr packages/.
composer remove emr/vuecrud
composer config --unset repositories.packagist
cd packages/emr/vuecrud
npm install 
npm run dev
```

make the change you want and run `npm run dev`

## 6. Go to main project 

`cd $HOME/proje1`

`php artisan migrate`

* vue_crud table test with tinker

`php artisan tinker` # test; See "null" message; because the table is empty; it is OK;

```
>>>Emr\VueCrud\VueCrud::all();
>>>q
```

`composer config repositories.local '{"type": "path", "url": "./packages/emr/vuecrud"}' --file composer.json`

`composer config repositories.packagist false`

`composer require emr/vuecrud` 

`php artisan vendor:publish --tag=public --force`

`sed -i 's/layouts/vuecrud/g' resources/views/home.blade.php`

`php artisan serve --host=94.177.187.77 --port=8000`

Test page:  `http://94.177.187.77:8000/web/vuecrud`



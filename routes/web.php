<?php

  // Main Group
	Route::group([
		'namespace' => "Emr\VueCrud\Http\Controllers",
		'prefix' => 'vuecrud',
		'middleware' => ['web'], ],  function () {

		// First page; http://{domain}/vuecrud/1
		 
		Route::get('/1', function () {
			return view('courier::vuecrud');
			});  
	
		// Vue page; with npm run dev
		Route::resource('crud', 'VueCrudController');
	
});


 

<?php
namespace Emr\VueCrud;  // src directory

use Illuminate\Support\ServiceProvider;
use Emr\VueCrud\Http\Controllers\VueCrudController;


class VueCrudServiceProvider extends ServiceProvider
{

	public function boot()
	{
		 // Automatic loading
		 $this->loadRoutesFrom( __DIR__ .'/../../routes/web.php');
		 $this->loadViewsFrom( __DIR__ .'/../../resources/views', 'courier'); // Use `courier::` in the Controllers section
		 $this->loadMigrationsFrom( __DIR__ .'/../../database/migrations');
		 
		// It is copy js and css to BASE {public_path} directory
		// php ../../../artisan vendor:publish --provider="Emr\VueCrud\VueCrudServiceProvider" --tag=public --force
		$this->publishes([
			__DIR__ . '/../../public/js' => public_path('vuecrud/js'),
			__DIR__ . '/../../public/css' => public_path('vuecrud/css'),
			//__DIR__ . '/../../resources/views' => base_path('resources/views'),
		], 'public');
		 
	}
	
	public function register()
	{
		
	}
    
}

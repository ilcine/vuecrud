<?php
namespace Emr\VueCrud\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request; 

use Emr\VueCrud\VueCrud;  // model name

class VueCrudController extends Controller
{
	
	/*
	public function vuecrud()
	{
		 
		return view('courier::vuecrud');
	}
	*/
	
  public function index()
  {
		$tasks = VueCrud::get();  

    return response()->json([
      'cruds'    => $tasks,
      'message' => 'Başarılı index !',
     ], 200);
  }
  
  public function create()
  {
      //
  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'product_name'  => 'required|max:40',
      'product_price' => 'required',
    ]);

    $task = new VueCrud;
    $task->product_name   = request('product_name');  
    $task->product_price  = request('product_price');  
    $task->save();

    return response()->json([
      'post'    => $task,
      'message' => "Successful Store ! $task ",
      'idGet' => $task->id
    ], 200);
  }

  public function show(VueCrud $crud)
  {
      //
  }
  
  public function edit(VueCrud $crud)
  {
      //
  }

  public function update(Request $request, VueCrud $crud)
  {
    $this->validate($request, [
      'product_name'     => 'required|max:40',
      'product_price' => 'required',
    ]);
     
    $crud->product_name  = request('product_name');  
    $crud->product_price = request('product_price');  
		$crud->save();  
		 
    return response()->json([
      'message' => "Successful Update! request:$request crud:$crud "
    ], 200);
  }
  
  public function destroy(VueCrud $crud)
  {
    $crud->delete();   
    return response()->json([
       'message' => "Successful Destroy $crud !"
    ], 200);
  }

}
